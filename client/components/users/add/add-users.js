// Open subscription
Meteor.subscribe('users');

Template.addUsersTemplate.events({

	"submit .addUserForm": function (event) {
		// This function is called when the add user form is submitted

		event.preventDefault();

		var firstNameField = $('.addUserForm input[name="firstName"]');

		var lastNameField = $('.addUserForm input[name="lastName"]');

		Meteor.call("createUser", { firstName: firstNameField.val(), lastName: lastNameField.val() });

		// Clear form
		lastNameField.val('') 
		firstNameField.val('');

		// Prevent default form submit
		return false;
	},

	
})