Meteor.subscribe('users');

Template.editUserTemplate.events({

	'submit .editUserForm': function(event)
	{
		event.preventDefault();
		
		var firstNameField = $('.editUserForm input[name="firstName"]');

		var lastNameField = $('.editUserForm input[name="lastName"]');

		Meteor.call('updateUser', this._id, firstNameField.val(), lastNameField.val())

		// Prevent default form submit
		return false;
		
	}
})