/**
 *
 * Route configuration for edit user path
 *
 */


Router.route('/users/edit/:_id', function()
{
	/**
	 * Route definition
	 * @param {string} templateName 
	 * @param {object} configuration
	 *
	 * @description Defines route for given path.  Data property defines what is injected into the view model.
	 */
	this.render('editUserTemplate', {
		data: function() {
			return Users.findOne({ _id: this.params._id })
		}
	});
},
{ 
	name: 'users.edit'
})