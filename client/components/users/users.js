
/**
 * Users 'Controller'
 */

/**
 * Subscribe to users publication
 */
Meteor.subscribe("users");


/**
 * Template helpers
 * @description Injects data points into the template so the view can render them
 */
Template.userList.helpers({
	users: function() {
		return Users.find({})
	}
})


/**
 * Template events
 * @description Defines event listeners on the template/view
 */
Template.userList.events({

	/**
	 * Delete User method
	 * 
	 * @description Deletes a user by ID
	 */
	"click .deleteUser": function () {
		Meteor.call("deleteUser", this._id);
	}
})