/**
 * Users collection
 * @description Defines our user collection on the DB
 * @type {Mongo}
 */
Users = new Mongo.Collection('users');


Users.allow({

	insert: function(payload)
	{

	}

})

/**
 * Helper methods
 * 
 * @description These essentially act as the model for your collection. 
 * You can expose methods here to interface with the collection.
 *
 * Available due to meteor-collection-helpers package
 *
 */
//Users.helpers({

	/**
	 * Get users
	 * @return {array} Array of all users
	 */
	/*getUsers: function()
	{
		return Users.find({});
	},


	/**
	 * Get User
	 * @param  {string} userId The primary key to search by ID
	 * @return {document}        Returns the document assocated with the ID
	 */
	/**getUser: function(userId)
	{
		// Locate the single document for this user
		var user = Users.findOne(userId);
		
		// Throw an exception if no document is found
		if (!user)
			throw new Meteor.Error("User not found!")

		// Otherwise return the user document
		return user;
	},

	createUser: function(firstName, lastName)
	{
		// Typecheck the inputs
		check(firstName, String);
		check(lastName, String);

		// Insert the record
		Users.insert({
			firstName: firstName,
			lastName: lastName,
			createdAt: new Date()
		})
	
	}
*/
//})