Meteor.methods({

	createUser: function(payload)
	{
		// Typecheck the inputs
		check(payload.firstName, String);
		check(payload.lastName, String);

		// Insert record
		Users.insert({
			firstName: payload.firstName,
			lastName: payload.lastName,
			createdAt: new Date()
		});
	},
	
	updateUser: function(userId, firstName, lastName)
	{
		Users.update(userId, { $set: { firstName: firstName, lastName: lastName } });
	},

	deleteUser: function(userId)
	{
		var user = Users.findOne(userId);

		if (!user)
			throw new Meteor.Error('Unable to locate user')

		Users.remove(userId);
	}

})

