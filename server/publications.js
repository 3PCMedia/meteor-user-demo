/**
 * Publications that are exposed to the UI
 */

/**
 * Publish USERS
 * @return {array}  Array of all users in collection USERS
 */
Meteor.publish("users", function () {
	return Users.find({});
});